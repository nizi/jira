# 一、git使用ssh密钥

## 1、安装git bash
            https://gitforwindows.org/，以下操作都在git bash中操作;
## 2、配置ssh密钥对
###     2.1 查看已有ssh密钥：
    打开Git Bash，$ cd ~/.ssh，若没有，则按照如下步骤创建；
###     2.2 创建新的ssh key:
    $ ssh-keygen -t rsa -C "your_email@youremail.com",直接按Enter就行。
### 2.3 复制ssh key到github：
    $ cat ~/.ssh/id_rsa.pub，查看公钥内容；
    登陆你的github帐户->点击你的头像-> Settings -> 左栏点击 SSH and GPG keys -> 点击 New SSH key； 
    复制上面的公钥内容，粘贴进“Key”文本域内；
    点击 Add key。
###     2.4 测试 ssh 链接 github：
    $ ssh -T git@gitlab.com 
    如果，看到：Hi xxx! You've successfully authenticated, but GitHub does not # provide shell access. 恭喜你，你的设置已经成功了。

# 二、将已有项目提交到git

## 方法一：
    1) 打开终端git bash，cd到已存在项目的目录   
    2) 初始化一个本地仓库
        ​git init
    3) 把工程所有文件都添加到该仓库中
        git add .
    4) 把文件提交到本地仓库
        git commit -m "Initial commit"​
    5) 添加远程仓库地址
        输入：git remote add origin + 你的仓库地址
    6) 把文件提交到远程仓库
        git push -u origin master​
    7)  参考url：https://blog.csdn.net/u013325929/article/details/70313414/
    8)  常见错误：
        1、GIT: error: pathspec 'xxx did not match any file(s) known to git
            解决方案：remove cached files 后再次提交
## 方法二：   
    Git global setup
        git config --global user.name "wangnini"
        git config --global user.email "1938885093@qq.com"

    Create a new repository
        git clone git@gitlab.com:nizi85/test.git
        cd test
        touch README.md
        git add README.md
        git commit -m "add README"
        git push -u origin master

    Existing folder
        cd existing_folder
        git init
        git remote add origin git@gitlab.com:nizi85/test.git
        git add .
        git commit -m "Initial commit"
        git push -u origin master
    
    Existing Git repository
        cd existing_repo
        git remote rename origin old-origin
        git remote add origin git@gitlab.com:nizi85/test.git
        git push -u origin --all
        git push -u origin --tags
## 一台机器多个git账号
    参考：https://www.cnblogs.com/popfisher/p/5731232.html

# 三、git基本操作


## 1、克隆代码到本地
    git clone git@git.uinnova.com:wangnini/frontAutoTest.git 
    
## 2、提交代码
    1) git add . 
    2) git commit -m "xxxx" 
    3) git push origin master 
    
## 3、查看代码分支
    git branch 

## 4、切换代码分支
    git branch <branch_name> 

# 三、测试用例设计原则

## 1、命名规范
    1、变量或方法命名具有可读性，不要随意命名，如ss
    2、遵循js命名规范

## 2、封装原则

### 2.1 变量
        1) 多次使用的元素Selector，请使用变量定义，统一封装到对象中

### 2.2 功能封装
        1) 基于html标签的通用组件操作，请封装到compentUtil.js中，如：input、表单提交
        2) 除1)外，还有一些共用的操作方式，请封装到commonUtil.js中，如：2级目录查找
        3) 对于各个功能的测试步骤，统一封装到各模块的**Util.js文件，且各功能步骤执行完成不遗留残余步骤，避免对后续操作造成影响
        4) 各模块特有的共用方法，统一封装到commonUtilFor**.js文件
        5) testSuite文件只只需要对2)定义的功能进行组合即可实现测试流程，不要过多将测试步骤暴露在这一层
        6) 目录组织结构请查看现有目录组织规则

### 2.3 case独立性
        1) 保持testSuit中每条case的独立性，即每个case可单独运行；
        2) 保证每个功能function的独立性，不对其他case的执行操作影响


        

    



