DROP PROCEDURE IF EXISTS wnntest ;
DELIMITER //
-- 修改为json
CREATE PROCEDURE wnntest( )
-- CREATE PROCEDURE wnntest(OUT res VARCHAR(2000) )
-- CREATE PROCEDURE getLastSixDaysBugs2( OUT pv VARCHAR(25), OUT cn FLOAT,OUT un FLOAT,out res Blob)
BEGIN
	-- 获取总量
	-- 判定是否结束循环
	DECLARE done BOOLEAN DEFAULT 0;
	-- declare cone boolean default 0;
	
	-- 定义结果集
	DECLARE total INT;
	DECLARE pv VARCHAR(25);
	DECLARE st DATE;
	DECLARE ft DATE;
	DECLARE cn FLOAT;
	-- out cn FLOAT;
	DECLARE un FLOAT;
	-- OUT un FLOAT;
	DECLARE tnum INT;
	DECLARE cnum INT;
	DECLARE unum INT;
	DECLARE  res VARCHAR(2000) DEFAULT '{';
	
	-- 定义游标
	-- DECLARE tCur CURSOR FOR SELECT  COUNT(*) AS total FROM buginfo GROUP BY Product_version;
	
	-- 游标初始化
	-- DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	-- DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET cone = 1;
	
	-- 打开游标
	/*OPEN tCur;
	
	-- 遍历结果集
	 REPEAT 
		FETCH tCur INTO total;
		IF done!=1 THEN
			SELECT total;
		END IF;
	UNTIL done END REPEAT;
	CLOSE tCur;*/
	
	-- 获取版本号对应的stime、ftime
	DECLARE rcCur CURSOR FOR SELECT product_version AS pv ,start_time AS st,finish_time AS ft FROM releasecycle GROUP BY product_version ORDER BY product_version;
	
	-- 游标初始化
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	OPEN rcCur;
	REPEAT 
		FETCH rcCur INTO pv,st,ft;
		IF done!=1 THEN
			-- select  pv,st,ft;
			-- 获取总量
		
			SELECT COUNT(*) INTO  tnum FROM buginfo WHERE Product_version = pv AND create_time>=st AND create_time<=ft;
			
			-- 后6天创建的数据
			
			SELECT COUNT(*) INTO  cnum FROM buginfo WHERE create_time>=DATE_SUB(ft,INTERVAL 8 DAY) AND Product_version=pv AND create_time<=ft;
			
			
			-- 后6天修复的数据
			SELECT COUNT(*) INTO  unum FROM buginfo WHERE fixed_status='完成' AND update_time>=DATE_SUB(ft,INTERVAL 8 DAY) AND Product_version=pv AND update_time<=ft  ;
			
			-- 修复比率、创建比率
			
			SET cn = ROUND(cnum/tnum,2);
			SET un = ROUND(unum/tnum,2);
			IF cn  IS NULL THEN
				SET cn=0;
			END IF;
			IF un  IS NULL THEN
				SET un=0;
			END IF;
			-- SELECT pv,st,ft,tnum,cnum,cn,unum,un;
			
			-- SET res = CONCAT('{"pv":"',pv,'"},');
			
			
			-- SET res = CONCAT(res,'{"pv":"',pv,',"cn":',cn,',"un":',un,'},');
			SET res = CONCAT(res,'"',pv,'"',':{"cn":',cn,',"un":',un,'},');
			-- SET res = CONCAT(res,'"cn":',cn,'},');
			-- SET res = CONCAT('{"un":"',un,'"},');
			
		END IF;
			
	UNTIL done END REPEAT;
	SET res=LEFT(res, LENGTH(res)-1);
	SET res= CONCAT(res,'}');
	
	CLOSE rcCur;
	
	SELECT res;
END;
//
DELIMITER ;
-- set @res='0';

CALL wnntest();
-- select res;

 