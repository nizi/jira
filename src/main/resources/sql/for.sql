
DROP PROCEDURE IF EXISTS GreetWorld2 ;
DELIMITER //
CREATE PROCEDURE GreetWorld2()
BEGIN
	-- 获取总量
	-- 判定是否结束循环
	DECLARE done BOOLEAN DEFAULT 0;
	-- 定义结果集
	DECLARE total INT;
	
	-- 定义游标
	DECLARE tCur CURSOR FOR SELECT  COUNT(*) AS total FROM buginfo GROUP BY Product_version;
	
	-- 游标初始化
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	
	-- 打开游标
	OPEN tCur;
	
	-- 遍历结果集
	 REPEAT 
		FETCH tCur INTO total;
		IF done!=1 THEN
		SELECT total;
		END IF;
	UNTIL done END REPEAT;
	CLOSE tCur;
END;
//
DELIMITER ;

 -- SET @greeting='Hello2'; 
 CALL GreetWorld2( ); 
 
