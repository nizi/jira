/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.7.24 : Database - jira
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`jira` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `jira`;

/*Table structure for table `releasecycle` */

DROP TABLE IF EXISTS `releasecycle`;

CREATE TABLE `releasecycle` (
  `product_version` varchar(255) NOT NULL,
  `start_time` varchar(255) DEFAULT NULL,
  `finish_time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`product_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `releasecycle` */

insert  into `releasecycle`(`product_version`,`start_time`,`finish_time`) values ('Tarsier_5.0.0','2018-08-17','2018-09-20'),('Tarsier_5.1.0','2018-09-21','2018-11-1'),('Tarsier_5.2.0','2018-11-2','2018-12-6'),('Tarsier_5.3.0','2018-12-8','2019-01-10');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
