DELIMITER $$

USE `jira`$$

DROP PROCEDURE IF EXISTS `getFixAndValidBugs`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getFixAndValidBugs`( )
BEGIN
	-- 获取总量
	-- 判定是否结束循环
	DECLARE done BOOLEAN DEFAULT 0;
	-- declare cone boolean default 0;
	
	-- 定义结果集
	DECLARE total INT;
	DECLARE pv VARCHAR(25);
	DECLARE st DATE;
	DECLARE ft DATE;
	DECLARE vn FLOAT;
	-- out cn FLOAT;
	-- DECLARE un FLOAT;
	-- OUT un FLOAT;
	DECLARE tnum INT;
	DECLARE vnum INT;
	DECLARE fnum INT;
	DECLARE  res VARCHAR(2000) DEFAULT '[';
	
	-- 定义游标
	-- DECLARE tCur CURSOR FOR SELECT  COUNT(*) AS total FROM buginfo GROUP BY Product_version;
	
	-- 游标初始化
	-- DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	-- DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET cone = 1;
	
	-- 打开游标
	/*OPEN tCur;
	
	-- 遍历结果集
	 REPEAT 
		FETCH tCur INTO total;
		IF done!=1 THEN
			SELECT total;
		END IF;
	UNTIL done END REPEAT;
	CLOSE tCur;*/
	
	-- 获取版本号对应的stime、ftime
	DECLARE rcCur CURSOR FOR SELECT product_version AS pv ,start_time AS st,finish_time AS ft FROM releasecycle WHERE product_version>='Tarsier_3.6.0' GROUP BY product_version ORDER BY product_version;
	
	-- 游标初始化
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	OPEN rcCur;
	REPEAT 
		FETCH rcCur INTO pv,st,ft;
		IF done!=1 THEN
			-- select  pv,st,ft;
			-- 获取总量
		
			SELECT COUNT(*) INTO  tnum FROM buginfo WHERE Product_version = pv AND create_time<=ft;
			
			-- 有效的数据
			
			SELECT COUNT(*) INTO  vnum FROM buginfo WHERE fixed_status<>'非问题关闭' AND create_time BETWEEN st AND ft AND Product_version=pv  ;
			
			
			-- 修复的数据
			SELECT COUNT(*) INTO  fnum FROM buginfo WHERE fixed_status<>'open' AND Product_version=pv AND update_time<=ft ;
			
			-- select vnum;
			-- 修复比率、创建比率
			
			-- SET cn = TRUNCATE(vnum/tnum,2);
			-- SET un = ROUND(unum/tnum,2);
			/*IF vnum  IS NULL THEN
				SET vnum=0;
			END IF;
			IF fnum  IS NULL THEN
				SET fnum=0;
			END IF;
			
			
			-- SET res = CONCAT('{"pv":"',pv,'"},');
			
			
			-- SET res = CONCAT(res,'{"pv":"',pv,',"cn":',cn,',"un":',un,'},');*/
			-- SELECT pv,st,ft,fnum,vnum,vnum/tnum,tnum,fnum/tnum;
			SET res = CONCAT(res,'{"pv":"',pv,'","vn":',ROUND(vnum/tnum,2),',"fn":',ROUND(fnum/tnum,2),'},','fnum:',fnum,'"total":',tnum);
			-- select vnum,tnum,pv;
			-- set res = concat(res,TRUNCATE(vnum/tnum*100,2),'%');
			-- SET res = CONCAT(res,vn);
			-- set res = concat(res,',"fn":');
			-- set res = CONCAT(res,TRUNCATE(fnum/tnum*100,2),'%},');
			-- set res = concat(res,'},');
			-- SET res = CONCAT(res,'"cn":',cn,'},');
			-- SET res = CONCAT('{"un":"',un,'"},');
			
		END IF;
			
	UNTIL done END REPEAT;
	SET res=LEFT(res, LENGTH(res)-1);
	SET res= CONCAT(res,']');
	
	CLOSE rcCur;
	
	SELECT res;
END$$

DELIMITER ;

CALL getFixAndValidBugs();