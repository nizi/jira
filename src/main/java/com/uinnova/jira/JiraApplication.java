package com.uinnova.jira;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
@MapperScan("com.uinnova.jira.mapper")
public class JiraApplication {

	public static void main(String[] args) {
		SpringApplication.run(JiraApplication.class, args);
	}
}
	
//public class JiraApplication extends SpringBootServletInitializer{
//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
//		return builder.sources(JiraApplication.class);
//	}
//
//
//	public static void main(String[] args) {
//		SpringApplication.run(JiraApplication.class, args);
//	}
//}
