package com.uinnova.jira.mapper;

import java.util.List;
import java.util.Map;

import com.uinnova.jira.model.Bug;

public interface BugMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Bug record);

    int insertSelective(Bug record);

    Bug selectByPrimaryKey(Integer id);
    
    //获取所有版本号
    String[] selectAllVersion();

    //获取所有产品线名称
    String[] selectAllProductNames();
    
    //获取各产品所有版本的bug总数
    List<Map<String, Object>> selectBugTotalByProductNameAndVersion(String pName);
    
    //获取各产品所有版本的bug总数
    List<Map<String, Object>> selectBugTotalByVersion();
    
    
    //获取版本修复时间
    List<Map<String, Object>> selectFixedTime();
    
    //获取后6天的创建时间
    //List<Map<String, Object>> getLastSixDaysBugs();
    
    List<Map<String, Object>> selectLastSomeDaysBug(int days);
    
    List<Map<String, Object>> selectFixAndValidBugs();
    
    List<Map<String, Object>> selectPreTestQuality(String productName);
    List<Map<String, Object>> selectPriorityByProduct(String version);
    List<Map<String, String>> selectPriorityType();
    
    List<Map<String, String>> selectVersionDays(String version);
    List<Map<String, Object>> selectCreateNum(String version);
    List<Map<String, Object>> selectFixedNum(String version);
//    List<Map<String, Object>> selectUpdateNum(String version);
    
    List<Map<String, Object>> selectBugsByDev(String version);
    List<Map<String, Object>> selectBugsByDevForYears();
    List<Map<String, Object>> selectBugsByReporter(String version);
    List<Map<String, Object>> selectBugsByReporterForYears();
    List<Map<String, Object>> selectOpenedBugs(String version);
    

    int updateByPrimaryKeySelective(Bug record);

    int updateByPrimaryKey(Bug record);
    
}