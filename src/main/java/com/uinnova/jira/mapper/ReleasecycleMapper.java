package com.uinnova.jira.mapper;

import java.util.List;
import java.util.Map;

import com.uinnova.jira.model.Releasecycle;

public interface ReleasecycleMapper {
    int deleteByPrimaryKey(String productVersion);

    int insert(Releasecycle record);

    int insertSelective(Releasecycle record);

    Releasecycle selectByPrimaryKey(String productVersion);
    
    List<Releasecycle> selectAll();
    
    String[] selectAllVersions();

    int updateByPrimaryKeySelective(Releasecycle record);

    int updateByPrimaryKey(Releasecycle record);
}