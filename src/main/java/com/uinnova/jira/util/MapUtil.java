package com.uinnova.jira.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class MapUtil  {

	public static String[] sortMap( String[] vs){
		
	        String verStr;
	        ArrayList<String> verArr = new ArrayList<String>();
	        
	        //去掉产品名称
	        for(int i=0;i<vs.length;i++) {
	        	String key = (String)vs[i];
	        	verStr = key.split("_")[1];
	        	System.out.println(verStr);
	        	
	        	verArr.add(verStr);
	        }
	        
	        String[] verSortArr= sortVer(verArr);
	        
	        //排序后的添加产品名称
	        String[] verStrArr = new String[vs.length];
	        
	        for(int i=0;i<verSortArr.length;i++) {
	        	verStrArr[i]="Tarsier_"+verSortArr[i];
	        }
	        System.out.println("产品版本：："+verStrArr);
	        
	        return verStrArr;
	}
	
	
	
	/**
	 * @param verArr 版本号数组
	 * @return 排序后的版本号数组
	 * 
	 */
	
	public static String[] sortVer(ArrayList<String> verArr) {
		
		//将版本号的ArrayList 转换为String[]
		String[] va = new String[verArr.size()];
		for(int i=0;i<va.length;i++) {
			va[i] = verArr.get(i);
		}
		
		String[] before;
		String[] after ;
		String tmp;
		
		for (int i=0;i<va.length;i++) {
			for(int j=0;j<va.length-1;j++) {
				 before = va[j].split("\\.");
				 after = va[j+1].split("\\.");
				//版本号分段排序
				if(Integer.parseInt(before[0])>Integer.parseInt(after[0])) {
					tmp = va[j];
					va[j]=va[j+1];
					va[j+1]=tmp;
				} else if(Integer.parseInt(before[0])==Integer.parseInt(after[0]) && Integer.parseInt(before[1])>Integer.parseInt(after[1])){
						tmp = va[j];
						va[j]=va[j+1];
						va[j+1]=tmp;
						
				} else if(Integer.parseInt(before[0])==Integer.parseInt(after[0]) && Integer.parseInt(before[1])==Integer.parseInt(after[1]) && Integer.parseInt(before[2])>Integer.parseInt(after[2])) {
							tmp = va[j];
							va[j]=va[j+1];
							va[j+1]=tmp;
						}
					}
				}
		System.out.println(va.toString());
		return va;		
	}
	
	public static List<Map<String, Object>> sortListMap(List<Map<String, Object>> mapList) {
		
		String pv,tmp ;
		ArrayList<String> vers = new ArrayList<String>();
		
		//从jsonarr中解析版本号
		for(int i=0;i<mapList.size();i++) {
			pv = (String) mapList.get(i).get("pv");
			tmp = pv.split("_")[1];
			vers.add(tmp);
		
		}
		
		//版本号排序
		String[] sortVers = MapUtil.sortVer(vers);
		String ver;
		Map<String,Object> map;
		
		//组装按版本号有序的jsonArr
		List<Map<String,Object>> sortListMap = new ArrayList<Map<String,Object>>();
		
		for (int i=0; i<sortVers.length;i++) {
			ver = "Tarsier_"+sortVers[i];
			for(int j=0;j<mapList.size();j++) {
				map = mapList.get(j);
				if(((String)map.get("pv")).equals(ver)) {
					sortListMap.add(map);
			}
		
		}
		}
		System.out.println(sortListMap);
		return sortListMap;
	}
	
	}
