package com.uinnova.jira.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class HttpRequestUtil {
	
	private static final   CloseableHttpClient httpclient =  HttpClients.createDefault();

	public String sendGet(String url) {
	
		HttpGet httpGet = new HttpGet(url);
		CloseableHttpResponse response = null;
		
		try {
			 response = httpclient.execute(httpGet);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String result = null;
		HttpEntity entity =  response.getEntity();
		try {
			if(entity!=null) {
				result = EntityUtils.toString(entity);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}

	
	public static Map<String,Object> sendPost(String url,Map<String,String> map,Map<String,String> headers) {
		
		
		//添加请求体
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		for(Map.Entry<String, String> entry:map.entrySet()) {
			params.add(new BasicNameValuePair(entry.getKey(),entry.getValue()));
		}
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params,Consts.UTF_8);
		HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(entity);
		
		
		//添加请求头
		if(headers!=null) {
			Set<Map.Entry<String,String>> entrySet = map.entrySet();
			Iterator<Map.Entry<String,String>> it = entrySet.iterator();
			
			while(it.hasNext()) {
				Map.Entry<String, String> me = it.next();
				String key = me.getKey();
				String value = me.getValue();
				System.out.println("==="+key+"===="+value);
				httpPost.addHeader(key, value);
				
			}
			System.out.println("====header===::"+httpPost.getHeaders("JSESSIONID"));
		}
		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpPost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		HttpEntity entity1 = response.getEntity();
		
		
		String result = null;
		try {
			result = EntityUtils.toString(entity1);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Map<String,Object> resMap = new HashMap();
		Header[] header = response.getAllHeaders();
		resMap.put("header", header);
		resMap.put("response", result);
		return resMap;
	}
	
	public static String ResultToFile(String response,String filePath)  {
		FileOutputStream out = null;
		try {
			File file =  new File(filePath);
			out =  new FileOutputStream(file);
			
//			FileInputStream in = new FileInputStream(response);
			byte[] resBytes = response.getBytes();
				 try {
					out.write(resBytes);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
//				out.flush();
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}
