package com.uinnova.jira.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.ibatis.javassist.compiler.ast.Symbol;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class StrUtil {

	public static  String  getSpecStr(String str,String startStr ,String endStr) {
		int start = str.indexOf(startStr);
		int end = str.indexOf(endStr);
		System.out.println("start::"+start+"  end::"+end);
		return str.substring(start+1, end);
	}
	
	//查找匹配字符串的index
	public static ArrayList getDistStrIndex(String str,String disStr) {
		Pattern r = Pattern.compile(disStr);
		Matcher m = r.matcher(str);
		ArrayList<String> arrValue = new ArrayList();
		int i = 0;
		String attrValue =null;
		
		//查找所有匹配元素
		while(m.find()){
			System.out.println(m.group());
			
		}
		return arrValue;
	}
	
	public static ArrayList findSpecStrIndex(String str,String cha){
		
		ArrayList<Integer> list =  new ArrayList<Integer>();
		//获取首次出现的位置
	    int  index=str.indexOf(cha);
	    String subStr = str.substring(index+cha.length());
	    list.add(index);
	    //获取后续出现的次数
	    while(subStr.contains(cha)) {
	    	//从子串中获取指定字符串index
	    	index = subStr.indexOf(cha);
	    	//继续获取从新的index开始的子串
	    	subStr = subStr.substring(index+cha.length());
    	    list.add(list.get(list.size()-1)+index+cha.length());
	    }
	    System.out.println(list);
	    System.out.println(list.size());
	return list;
	}
	
	public static Date getDate(String str) {
		String s =  "2018-12-03";
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    Date date = null;
	    try {
	    	date = sdf.parse(str);
			System.out.println(sdf.parse(str).toLocaleString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return date;
	}
	
	//获取时间差
	public static int getTimeStampe(String fromStr,String toStr) {
//		String fromStr =  "2018-12-03";
//		String toStr =  "2018-12-05";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        int days = 0 ;
        try {
//			System.out.println(s);
        	long from = sdf.parse(fromStr).getTime();
        	long to = sdf.parse(toStr).getTime();
        	System.out.println("from::"+from+"::"+to);
			days = (int) ((to - from)/(1000 * 60 * 60 * 24));	
			System.out.println(days);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return days;
	}
	
	//将list<Map<String,Integer>>转换为Map<String,Integer>
	public static Map<String, Integer> listmapToMap(List<Map<String, Object>> list,String[] pVersions) {
		
		//将{"Product_version"=Product_version,"num"=num}组装成{"Product_version"=num}
		Map<String, Integer> vn =  new HashMap<String,Integer>();
		
		for(int j=0;j<list.size();j++) {
			Map<String,Object> map = list.get(j);
			String v = (String) map.get("Product_version");
			int num = ((Number) map.get("num")).intValue();
			vn.put(v, num);
		}
		
		
		//对于某些版本没有值得数据，补充0.
		Map<String, Integer> vns =  new HashMap<String,Integer>();
		for(int i=0;i<pVersions.length;i++) {
			System.out.println("==获取当前版本数据==="+pVersions[i]);
			if (vn.get(pVersions[i])==null) {
				vns.put(pVersions[i], 0);
			}else {
				vns.put(pVersions[i], vn.get(pVersions[i]));
			}
		}
//		Map<String, Integer> sortMap = null;
//		sortMap=MapUtil.sortMap(vn);
//		
		System.out.println(vns);
		return vns;
//		return sortMap;
	}

	public static JSONObject listMapToJson(List<Map<String, Object>> list,String keyAttr,String valAttr){
		
		String key = null;
		String val = null;
		int num = 0;
		JSONObject obj = new JSONObject();
		for(int i=0;i<list.size();i++) {
			key = list.get(i).get(keyAttr).toString();
			
			val = list.get(i).get(valAttr).toString();
			try {
				if(valAttr.contains("num")) {
					num = Integer.parseInt(val);
						obj.put(key, num);
					
				}else {
					obj.put(key, val);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return obj;
	}
	
	public static JSONArray str2JsonArray(String str) {
		
		//只适用于[{},{}]的格式,将str转换成str[]
		String[] s = str.substring(1,str.length()-1).split("},");
		
		for(int i=0;i<s.length-1;i++) {
			s[i]=s[i]+"}";
		}
		System.out.println(s);
		//拆分每个jsonobjectStr to JSONObject
		
		String[] tmp,sp;
		JSONArray ja = new JSONArray();
		DecimalFormat df = new DecimalFormat("0.##");
		
		for(int j=0;j<s.length;j++) {
			tmp = s[j].substring(1,s[j].length()-1).replace("\"", "").split(",");
			
			JSONObject obj = new JSONObject();
			
			for (int k=0;k<tmp.length;k++) {
				sp = tmp[k].split(":");
				System.out.println(sp[0]+" "+sp[1]);
				try {
					if(sp[0].equals("pv")) {
						obj.put(sp[0], sp[1]);
					}else {
						obj.put(sp[0],	Float.valueOf(df.format(Float.parseFloat(sp[1]))));
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			ja.put(obj);
		}
		System.out.println("ja::"+ja.toString());
		
		return ja ;
		
	}
	
	public static JSONArray sortJsonArrayByVersion(JSONArray ja) {
		
		//获取版本号并排序
		ArrayList<String> verArr = new ArrayList<String>();
		String pv ;
		for(int i=0;i<ja.length();i++) {
			try {
				pv = (String)ja.getJSONObject(i).get("pv");
				verArr.add(pv.split("_")[1]);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String[] verStr = MapUtil.sortVer(verArr);
		
		//按照版本号顺序重组jsonArr
		
		JSONObject obj = null;
		String tmp;
		JSONArray sortArr = new JSONArray();
		
		for(int i=0;i<verStr.length;i++) {
			for(int j=0;j<ja.length();j++) {
				try {
					obj = ja.getJSONObject(j);
					tmp = ((String)obj.get("pv")).split("_")[1];
					if(tmp.equals((String)verStr[i])){
						sortArr.put(obj );
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return sortArr;
		
	}
}
