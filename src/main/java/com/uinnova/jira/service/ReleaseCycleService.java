package com.uinnova.jira.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.uinnova.jira.model.Releasecycle;


@Service
public interface ReleaseCycleService {
	
	public List<Releasecycle> getAll();
	public String[] getAllVers();
	
}
