package com.uinnova.jira.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.uinnova.jira.model.Bug;

@Service
public interface BugService {
	
	 int addBug(Bug bug);

	 int updateBug(Bug bug);
	 
//	 ArrayList<String> getAllVersion();
	 String[] getAllVersion();
	 
	 String[] getAllProductNames();
	 
	 Map<String,Integer> getBugTotalByProductNameAndVersion(String pName ,String[] versions);
	 
//	 List<Map<String,Object>> getBugTotalByVersion();
	 
	 public  List<Map<String, Object>> getFixedTime();
	 
	 public  List<Map<String, Object>> getLastSomeDaysBugPer(int days);
	 
	 public  List<Map<String, Object>> getFixAndValidBugsPer();
	 
	 public  List<Map<String, Object>> getPreTestQuality(String productName);
	 public  List<Map<String, Object>> getPriorityByVersion(String version);
	 public  List<Map<String, String>> getPriorityType();
	 
	 public  List<Map<String, String>> getVersionDays(String version);
	 public  List<Map<String, Object>> getCreateNum(String version);
	 public  List<Map<String, Object>> getFixedNum(String version);
//	 public  List<Map<String, Object>> getUpdateNum(String version);
	 
	 public  List<Map<String, Object>> getBugsByDev(String version);
	 public  List<Map<String, Object>> getBugsByDevForYears();
	 public  List<Map<String, Object>> getBugsByReporter(String version);
	 
	 public  List<Map<String, Object>> getBugsByReporterForYears();
	 
	 public  List<Map<String, Object>> getOpenedBugs(String version);
//	 List<Bug> findAllBugs();

}
