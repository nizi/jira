package com.uinnova.jira.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uinnova.jira.mapper.BugMapper;
import com.uinnova.jira.model.Bug;
import com.uinnova.jira.service.BugService;
import com.uinnova.jira.util.MapUtil;
import com.uinnova.jira.util.StrUtil;

@Service(value="BugService")
public class BugServiceImpl implements BugService {

	@Autowired
	private BugMapper bugMapper;
	
	@Override
	public int addBug(Bug bug) {
		return bugMapper.insert(bug);
		
	}

	@Override
	public int updateBug(Bug bug) {
		//按照id查找,确认是否存在此id，如果有，则更新，否则插入
		int i = 0 ;
		Bug bugRec = bugMapper.selectByPrimaryKey(bug.getId());
		System.out.println("====查找库里是否有此数据==="+bugRec);
		if(bugRec==null) {
			addBug(bug);
			System.out.println("====新数据==="+bug.getId());
		}else {
			i = bugMapper.updateByPrimaryKey(bug);
			System.out.println("====已有数据更新===="+i);
			
		}
		return i;
	}

	@Override
//	public ArrayList<String>  getAllVersion() {
		public String[]  getAllVersion() {
		
		String[] vs = bugMapper.selectAllVersion();
		System.out.println("===="+vs);
		//去掉所有“空”版本
		ArrayList<String> versions = new ArrayList<String>();
		int j=0;
		for(int i=0;i<vs.length;i++) {
			if (vs[i].contains("空")) {
				continue;
			}
			versions.add(vs[i]);
			j++;
			System.out.println("===i="+i+"==j=="+j);
		}
		
		String[] v = (String[]) versions.toArray(new String[versions.size()]);
		System.out.println(v);
		
		return v;
	}

	@Override
	public String[] getAllProductNames() {
		String[] pNames = bugMapper.selectAllProductNames();
		return pNames;
	}

	@Override
	public Map<String, Integer> getBugTotalByProductNameAndVersion(String pName,String[] vs) {
		
		Map<String, Integer> pt = null;
		
		List<Map<String, Object>> ptLi;
		if(pName!=null) {
			//按产品线获取版本号
			ptLi = bugMapper.selectBugTotalByProductNameAndVersion(pName);
		}else {
			ptLi = bugMapper.selectBugTotalByVersion();
		}
		pt = StrUtil.listmapToMap( (List<Map<String, Object>>) ptLi, vs);
		System.out.println(pt);
		return pt;
	}

//	@Override
//	public List<Map<String, Object>> getBugTotalByVersion() {
//		return bugMapper.selectBugTotalByVersion();
//	}

	@Override
//	public String getFixedTime(String startTime, String finishTime) {
//		return bugMapper.selectFixedTime(startTime, finishTime);
//	}
	public  List<Map<String, Object>> getFixedTime() {
		return bugMapper.selectFixedTime();
	}

//	@Override
//	public List<Map<String, Object>> getLastSixDaysPer() {
//		
//		return bugMapper.getLastSixDaysBugs();
//	}
	
	@Override
	public List<Map<String, Object>> getLastSomeDaysBugPer(int days) {
		
		return bugMapper.selectLastSomeDaysBug(days);
	}

	@Override
	public List<Map<String, Object>> getFixAndValidBugsPer() {
		return bugMapper.selectFixAndValidBugs();
	}

	@Override
	public List<Map<String, Object>> getPreTestQuality(String productName) {
		return bugMapper.selectPreTestQuality(productName);
	}

	@Override
	public List<Map<String, Object>> getPriorityByVersion(String version) {
		return bugMapper.selectPriorityByProduct(version);
	}

	@Override
	public List<Map<String, String>> getPriorityType() {
		return bugMapper.selectPriorityType();
	}

	@Override
	public List<Map<String, Object>> getCreateNum(String version) {
		return bugMapper.selectCreateNum(version);
	}

	@Override
	public List<Map<String, String>> getVersionDays(String version) {
		return bugMapper.selectVersionDays(version);
	}

	@Override
	public List<Map<String, Object>> getFixedNum(String version) {
//		return bugMapper.selectUpdateNum(version);
		return bugMapper.selectFixedNum(version);
	}

	@Override
	public List<Map<String, Object>> getBugsByDev(String version) {
		return bugMapper.selectBugsByDev(version);
	}
	
	@Override
	public List<Map<String, Object>> getBugsByDevForYears() {
		return bugMapper.selectBugsByDevForYears();
	}
	
	@Override
	public List<Map<String, Object>> getBugsByReporter(String version) {
		return bugMapper.selectBugsByReporter(version);
	}

	@Override
	public List<Map<String, Object>> getBugsByReporterForYears() {
		return bugMapper.selectBugsByReporterForYears();
	}

	@Override
	public List<Map<String, Object>> getOpenedBugs(String version) {
	
		return bugMapper.selectOpenedBugs(version);
	}

	
	
//	@Override
//	public List<Bug> findAllBugs() {
//		return bugMapper.selectAllBug();
//	}

}
