package com.uinnova.jira.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uinnova.jira.mapper.ReleasecycleMapper;
import com.uinnova.jira.model.Releasecycle;
import com.uinnova.jira.service.ReleaseCycleService;

@Service(value="ReleaseCycleService")
public class ReleaseCycleImpl implements ReleaseCycleService{

	@Autowired
	ReleasecycleMapper reCy;
	
	@Override
	public List<Releasecycle> getAll() {
		List<Releasecycle> rc = reCy.selectAll();
		return rc;
	}

	@Override
	public String[] getAllVers() {
		return reCy.selectAllVersions();
	}


}
