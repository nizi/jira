package com.uinnova.jira.JiraData;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.uinnova.jira.model.Bug;
import com.uinnova.jira.service.BugService;
import com.uinnova.jira.service.impl.BugServiceImpl;
import com.uinnova.jira.util.HttpRequestUtil;
import com.uinnova.jira.util.StrUtil;


public class GetBugData {
	static String host = "http://111.204.156.11:8070/";
//	static String host = "http://192.168.1.199:8080/";
	
//	public static void main(String[] args) throws JSONException {
//		
//		
//		ArrayList<String> cookie = getCookie(host);
//		String response  =  null;
//		
//		ArrayList list  =  new ArrayList();
//		for(int i=0;i<1;i++) {
//			//获取每页数据
//			 response = getBugInfoByPage(host,cookie,i);
////			//获取tobody数据
//			String tbody = StrUtil.getSpecStr(response, "<tbody>", "</tbody>");
//				for (int j=0;j<50;j++) {
////				//获取每页中的每行数据，并封装到jsonarr
//////					getRowAttrToJson(tbody, arr, j);
////				//获取每页中的每行数据，并封装到jsonobj中，以id为key
//////					getRowAttrToJson(tbody, obj, j);
////				//获取每页中的每行数据，并封装到json
//				getRowAttrToObj(tbody, list, j);
//			}
//		}
//	
//		String filePath ="D:\\wnn.html";
//		HttpRequestUtil.ResultToFile(response.toString(),filePath);
////		System.out.println("********************"+list.get(0));
//		
////		
////		getCookie(host);
////		getCookie(host);
//}
	
	public static ArrayList getBugsDataByPageToDb(String hostUrl) throws JSONException {
		ArrayList<String> cookie = getCookie(hostUrl);
		
		//将结果转成object，放入list，准备插入数据
		ArrayList list  =  new ArrayList();
		String info ;
		
		for(int i=0;i<1;i++) {
			//获取每页数据
			String response = getBugInfoByPage(hostUrl,cookie,i);
			//获取tobody数据
			String tbody = StrUtil.getSpecStr(response, "<tbody>", "</tbody>");
				for (int j=0;j<50;j++) {
				//获取每页中的每行数据，并封装到jsonarr
//					getRowAttrToJson(tbody, arr, j);
				//获取每页中的每行数据，并封装到jsonobj中，以id为key
//					getRowAttrToJson(tbody, obj, j);
				//获取每页中的每行数据，并封装到json
				getRowAttrToObj(tbody, list, j);
			}
		}
		return list;
	}


public static ArrayList getAllBugsData(String hostUrl) throws JSONException {
	ArrayList<String> cookie = getCookie(hostUrl);
	
	//将结果转成object，放入list，准备插入数据
	ArrayList list  =  new ArrayList();
	String info ;
	
	//获取所有数据
	for(int i=0;i<20;i++) {
		
		
		String response = getBugInfoByPage(hostUrl,cookie,i);
		//获取tobody数据
		String tbody = StrUtil.getSpecStr(response, "<tbody>", "</tbody>");
		System.out.println("===========tobody====\r\n"+tbody);
		
		//获取每页数据
		for (int j=0;j<50;j++) {
			
		//获取每页中的每行数据，并封装到jsonarr
//		getRowAttrToJson(tbody, arr, j);
		//获取每页中的每行数据，并封装到jsonobj中，以id为key
//		getRowAttrToJson(tbody, obj, j);
		//获取每页中的每行数据，并封装到json
		getRowAttrToObj(tbody, list, j);
		}
	}
	return list;
}

	//获取每行的属性信息,放入object
	private static void getRowAttrToObj(String tbody, ArrayList list, int i) throws JSONException {
			
		String info;
		
		//获取每行数据
		info = getRowInfo(tbody,i);
//		System.out.println(getRowInfo(tbody,i));
		
		//获取每个数据组装成json
		//1.获取id
		String issuerow_Pattern = "\"issuerow(\\d+)\"";
		String issuerow_startStr = "\"";
		String issuerow_endStr = "\"";
		String idStr = getRegAttr(info,issuerow_Pattern,issuerow_startStr,issuerow_endStr,9).get(0).toString();
		int id = Integer.parseInt(idStr);
		System.out.println("==============id============="+id);
		
		//2.key
		String issuekey_Pattern = "data-issuekey=\"(.*)-(\\d+)\"";
		String issuekey_startStr = "\"";
		String issuekey_endStr = "\"";
		String issuekey =getRegAttr(info,issuekey_Pattern,issuekey_startStr,issuekey_endStr,1).get(0).toString();
		System.out.println("============issuekey==============="+issuekey);
		
		//3.desc
		String desc_Pattern = "<a class=\"issue-link\" data-issue-key=\""+issuekey+"\" href=\"/browse/"+issuekey+"\">(.*)</a>";
		String desc_startStr = ">";
		String desc_endStr = "<";
		String desc =getRegAttr(info,desc_Pattern,desc_startStr,desc_endStr,1).get(1).toString();
		System.out.println("============desc==============="+desc);
		
		
		//4.type为故障
		//8.priority=====有故障、优先级2个，自动取后者
		String type_Pattern = "align=\"absmiddle\" alt=\"(.*)\" ";
		String type_startStr = "alt=\"";
		String type_endStr = "\"";
		String type =getRegAttr(info,type_Pattern,type_startStr,type_endStr,5).get(0).toString();
		System.out.println("==========type================="+type);
		
		 //6.blongProjectName
		String projectKey_Pattern = "<td class=\"project\">    <a href=\"/browse/(.*)\"";
		String projectKey_startStr = "e/";
		String projectKey_endStr = "\" ";
		String projectKey =getRegAttr(info,projectKey_Pattern,projectKey_startStr,projectKey_endStr,2).get(0).toString();
		System.out.println("=============projectKey=============="+projectKey);
//		//6.blongProjectName
//		String projectKey_Pattern = "<a href=\"/browse/(.*)\" class=\"tinylink\">";
//		String projectKey_startStr = "e/";
//		String projectKey_endStr = "\" ";
//		String projectKey =getRegAttr(info,projectKey_Pattern,projectKey_startStr,projectKey_endStr,2).get(0).toString();
//		System.out.println("=============projectKey=============="+projectKey);
		
		//7.projectName
		String projectName_Pattern = "<a href=\"/browse/"+projectKey+"\" class=\"tinylink\">(.*)</a>";
		String projectName_startStr = ">";
		String projectName_endStr = "<";
		String projectName =getRegAttr(info,projectName_Pattern,projectName_startStr,projectName_endStr,1).get(0).toString();
		System.out.println("============projectName==============="+projectName);
		
		//8.priority=====有故障、优先级2个，自动取后者
		String priority_Pattern = "align=\"absmiddle\" alt=\"(.*)\" ";
		String priority_startStr = "alt=\"";
		String priority_endStr = "\"";
		String priority =getRegAttr(info,priority_Pattern,priority_startStr,priority_endStr,5).get(1).toString();
		System.out.println("==========priority================="+priority);
		
		//8.fixedStatus ;">非问题关闭</span>
		String fixedStatus_Pattern = ";\">(.*)</span>";
		String fixedStatus_startStr = ">";
		String fixedStatus_endStr = "<";
		String fixedStatus =getRegAttr(info,fixedStatus_Pattern,fixedStatus_startStr,fixedStatus_endStr,1).get(0).toString();
//		if(fixedStatus.contains("<em>")) {
//			fixedStatus="未解决";
//		}
		System.out.println("============fixedStatus==============="+fixedStatus);
		
//		8.获取developer人员名称(中文)
		String devChina_Pattern = "id=\"assignee_(.*)\" ";
		String devChina_startStr = "_";
		String devChina_endStr = "\"";
		String devChina =getRegAttr(info,devChina_Pattern,devChina_startStr,devChina_endStr,1).get(0).toString();
		System.out.println("============dev==============="+devChina);

//		9.获取reporter人员名称(中文)
//		String reporter_Pattern = "ViewProfile.jspa(.*)</a>";
//		String reporter_startStr = ">";
//		String reporter_endStr = "<";
//		String reporter =getRegAttr(info,reporter_Pattern,reporter_startStr,reporter_endStr,1).get(1).toString();
//		System.out.println("============reporter==============="+reporter);
		
//		9.获取reporter人员名称(中文)
		
		String reporterEN_Pattern = "id=\"reporter_(.*)\" ";
		String reporterEN_startStr = "_";
		String reporterEN_endStr = "\"";
		String reporterEN =GetBugData.getRegAttr(info,reporterEN_Pattern,reporterEN_startStr,reporterEN_endStr,1).get(0).toString();
		//获取中文
		String reporter_Pattern = "id=\"reporter_(.*)</a>";
		String reporter_startStr = "\">";
		String reporter_endStr = "<";
		String reporter =GetBugData.getRegAttr(info,reporter_Pattern,reporter_startStr,reporter_endStr,2).get(0).toString();
		System.out.println("============reporter==============="+reporter);
		
		
		
			
		//10.获取创建、更新时间、已完成日期 2018-12-05T09:59:12+0800
//		String date_Pattern = " datetime=\"(.*)\"";
//		String date_startStr = "=\"";
//		String date_endStr = "T";
//		String created_time =getRegAttr(info,date_Pattern,date_startStr,date_endStr,2).get(0).toString();
//		String updated_time =getRegAttr(info,date_Pattern,date_startStr,date_endStr,2).get(1).toString();
//		
//		Date createdTime = StrUtil.getDate(created_time);
//		Date updatedTime = StrUtil.getDate(updated_time);
//		int fixedTimeStampe = StrUtil.getTimeStampe(created_time, updated_time);
		//创建日期
		String cdate_Pattern = " <td class=\"created\"> <span(.*)</time>";
		String cdate_startStr = "datetime=\"";
		String cdate_endStr = "T";
		String created_time =getRegAttr(info,cdate_Pattern,cdate_startStr,cdate_endStr,10).get(0).toString();
		System.out.println("============创建日期为==============="+created_time);
		//更新日期
		String udate_Pattern = " <td class=\"updated\"> <span(.*)</time>";
		String udate_startStr = "datetime=\"";
		String udate_endStr = "T";
		String updated_time =getRegAttr(info,udate_Pattern,udate_startStr,udate_endStr,10).get(0).toString();
		System.out.println("============更新日期为==============="+updated_time);
		//解决日期
		String fdate_Pattern = " <td class=\"resolutiondate\">(.*)</td>";
		String fdate_startStr = "<td ";
		String fdate_endStr = "</td>";
		String ftmp =getRegAttr(info,fdate_Pattern,fdate_startStr,fdate_endStr,10).get(0).toString();
		
		String fdate_time = "";
		if(ftmp.contains("<span ")) {
			fdate_startStr = "datetime=\"";
			fdate_endStr = "T";
			fdate_time =getRegAttr(info,fdate_Pattern,fdate_startStr,fdate_endStr,10).get(0).toString();
		}
		System.out.println("============解决日期为==============="+fdate_time);
		
		//修复时长
		Date createdTime = StrUtil.getDate(created_time);
		Date updatedTime = StrUtil.getDate(updated_time);
		Date fdatedTime = null;
//		int fixedTimeStampe = StrUtil.getTimeStampe(created_time, updated_time);
//		System.out.println("============修复时长为==============="+fixedTimeStampe);
		
		Integer fixedTimeStampe = null;
		
		if(!fdate_time.equals("")) {
			fdatedTime = StrUtil.getDate(fdate_time);
			fixedTimeStampe = StrUtil.getTimeStampe(created_time, fdate_time);
		}
		
		System.out.println("============修复时长为==============="+fixedTimeStampe);
		
		//11.获取version
		String version_Pattern = "<span>(.*)</span>";
		String version_startStr = ">";
		String version_endStr = "<";
		String version =getRegAttr(info,version_Pattern,version_startStr,version_endStr,1).get(0).toString();
		System.out.println("============version==============="+version);
		
//		11.获取stage
		String stage_Pattern = "  <td class=\"customfield_10500\">    (.*)";
		String stage_startStr = ">";
		String stage_endStr = "";
		String stage =getRegAttr(info,stage_Pattern,stage_startStr,stage_endStr,5).get(0).toString();
		System.out.println("============stage==============="+stage);
		
		Bug bug = new Bug(id,issuekey,desc,type,projectKey,projectName,priority,fixedStatus,devChina,reporter,createdTime,fdatedTime,updatedTime,
				 fixedTimeStampe,version,stage);
		System.out.println("********************"+bug);
		list.add(bug);
	}

	//从请求返回的信息中获取指定行的信息
	private static String getRowInfo(String tbody,int rowNum) {
		
	ArrayList startTr = StrUtil.findSpecStrIndex(tbody,"<tr");
	ArrayList endTr = StrUtil.findSpecStrIndex(tbody,"</tr>");
	ArrayList<Integer> s = new ArrayList() ;
	int num;
	
	//获取每行bug信息
	System.out.println("====startTr==="+startTr);
	System.out.println("====endTr==="+endTr);
//	System.out.println("==========tbody=========="+tbody);
	String info = tbody.substring((int)startTr.get(rowNum), (int)endTr.get(rowNum));
//	System.out.println("============\r\n"+info);
	return info;
}

	//通过取匹配规则获取每个属性值
	public  static ArrayList getRegAttr(String str,String reg,String startStr,String endStr,int increase) {
		
		Pattern r = Pattern.compile(reg);
		Matcher m = r.matcher(str);
		
		ArrayList<String> arr = new ArrayList();
		String attrValue = null;
		
		while(m.find()){
			
			System.out.println("::matchered::"+m.group());
			 int startIndex = m.group().indexOf(startStr);
			 int lastIndex = m.group().lastIndexOf(endStr);
			 System.out.println("strIndex::"+startIndex+"::endindex::"+lastIndex);
			 attrValue = m.group().substring(startIndex+increase, lastIndex);
			System.out.println("::value::"+attrValue);
			arr.add(attrValue);
		}
		if(attrValue==null) {
			System.out.println("未匹配到指定字符串"+reg);
			if(reg=="<span>(.*)</span>") {
				arr.add("空");
			}
			if(reg=="id=\"assignee_(.*)\" ") {
				arr.add("未分配");
			}
			if(reg=="  <td class=\"customfield_10500\">    (.*)") {
				arr.add("无");
			}
			
				
		}
		System.out.println("=============此属性匹配完成=======");
		return arr;
	}


	//获取首页请求数据
	private static String getBugInfoByPage(String hostUrl,ArrayList<String> cookie,int pageNum) {
		String url = hostUrl+"issues/?filter=11615";
		System.out.println("=======url"+url);
		if(pageNum!=0) {
			url =url+"&startIndex="+pageNum*50;
		}
		System.out.println(url); 
		Map<String,String> params = new HashMap<>();
		Map<String,String> headers = new HashMap<>();
		params.put("startIndex","0");
		params.put("filterId","11615");
		params.put("jql","project in (BASE, DMV001, EMV001, PMV001, DIX, CMV001, DCV001, SMV) AND issuetype = 故障 ORDER BY created DESC, summary DESC");
		params.put("layoutKey","list-view");
		//判断是否cookie有效时调用
		if(cookie.size()!=0) {
			headers.put("Cookie","JSESSIONID="+cookie.get(0)+"; atlassian.xsrf.token="+cookie.get(1));
		}
		headers.put("__amdModuleName","jira/issue/utils/xsrf-token-header");
		headers.put("X-Atlassian-Token","no-check");
		headers.put("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
//		headers.put("Host","text/plain");
		String  response=  (String) HttpRequestUtil.sendPost(url, params, headers).get("response");
		System.out.println("========response===="+pageNum+"========response====\r\n"+response);
		return response;
	}
		
	//获取请求cookie
	private static ArrayList<String> getCookie(String hostUrl) {
		
		ArrayList<String> list = new ArrayList();
		
		String url = hostUrl+"login.jsp";
		System.out.println("===getcookie===="+url);
		Map<String,String> params = new HashMap<>();
		params.put("os_username","wangnini");
		params.put("os_password","AaBb123_");
		Header[] header = (Header[]) com.uinnova.jira.util.HttpRequestUtil.sendPost(url, params,null).get("header");
		for(int i=0;i<header.length;i++) {
			System.out.println("======get response header ===="+header[i]);
		}
		
		String jessoinId = header[5].toString();
		String token = header[7].toString();
		System.out.println(jessoinId);
		System.out.println(token);
		if(jessoinId.contains("Set-Cookie")) {
			System.out.println(StrUtil.getSpecStr(jessoinId, "=", ";"));
			System.out.println(StrUtil.getSpecStr(token, "=", ";"));
			
			list.add(jessoinId);
			list.add(token);
		}else {
			System.out.println("系统cookie仍然有效！！！");
		}
		
		return list;
	}
		
	}

