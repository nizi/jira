package com.uinnova.jira.JiraData;

import java.util.ArrayList;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.uinnova.jira.model.Bug;
import com.uinnova.jira.service.BugService;

@Component
public class schedule {

	@Autowired
	BugService bugService;
	
	
	@Scheduled(cron="0 0 0 * * *")
//	@Scheduled(cron="0 0/30 * * * *")
//	@Scheduled(cron="0/10 * * * * *")
	//同步jira数据到mysql中
	public void cronJob() {

		String host = "http://111.204.156.11:8070/";
		ArrayList<Bug> list = null;

		try {
//			获取jirabug
			list = GetBugData.getAllBugsData(host);
			System.out.println("=====================insert start=================================");
				for (int i=0;i<list.size();i++) {
					System.out.println("****insertDB**"+list.get(i).getId());
//					bugService.addBug(list.get(i));
					//更新mysql中的数据
					bugService.updateBug(list.get(i));
					Thread.currentThread().sleep(10);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("sucess");
	}
		
}
