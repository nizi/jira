package com.uinnova.jira.model;

import java.util.Date;

public class Bug {
    private Integer id;

	private String issueKey;

    private String summary;

    private String type;

    private String projectKey;

    private String projectName;

    private String priority;

    private String fixedStatus;

    private String developer;

    private String reporter;

    private Date createTime;
    
    private Date fixedTime;

    private Date updateTime;

    private Integer fixedTimestamp;

    private String productVersion;

    private String bugStage;

	public Bug() {
		super();
	}
    
    public Bug(Integer id, String issueKey, String summary, String type, String projectKey, String projectName,
			String priority, String fixedStatus, String developer, String reporter, Date createTime,Date fixedTime,Date updateTime,
			Integer fixedTimestamp, String productVersion, String bugStage) {
		super();
		this.id = id;
		this.issueKey = issueKey;
		this.summary = summary;
		this.type = type;
		this.projectKey = projectKey;
		this.projectName = projectName;
		this.priority = priority;
		this.fixedStatus = fixedStatus;
		this.developer = developer;
		this.reporter = reporter;
		this.createTime = createTime;
		this.fixedTime = fixedTime;
		this.updateTime = updateTime;
		this.fixedTimestamp = fixedTimestamp;
		this.productVersion = productVersion;
		this.bugStage = bugStage;
	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIssueKey() {
        return issueKey;
    }

    public void setIssueKey(String issueKey) {
        this.issueKey = issueKey == null ? null : issueKey.trim();
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary == null ? null : summary.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey == null ? null : projectKey.trim();
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName == null ? null : projectName.trim();
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority == null ? null : priority.trim();
    }

    public String getFixedStatus() {
        return fixedStatus;
    }

    public void setFixedStatus(String fixedStatus) {
        this.fixedStatus = fixedStatus == null ? null : fixedStatus.trim();
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer == null ? null : developer.trim();
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter == null ? null : reporter.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
    public Date getFixedTime() {
        return fixedTime;
    }

    public void setFixedTime(Date fixedTime) {
        this.fixedTime = fixedTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getFixedTimestamp() {
        return fixedTimestamp;
    }

    public void setFixedTimestamp(Integer fixedTimestamp) {
        this.fixedTimestamp = fixedTimestamp;
    }

    public String getProductVersion() {
        return productVersion;
    }

    public void setProductVersion(String productVersion) {
        this.productVersion = productVersion == null ? null : productVersion.trim();
    }

    public String getBugStage() {
        return bugStage;
    }

    public void setBugStage(String bugStage) {
        this.bugStage = bugStage == null ? null : bugStage.trim();
    }
}