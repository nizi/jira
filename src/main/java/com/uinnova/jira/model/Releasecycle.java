package com.uinnova.jira.model;

public class Releasecycle {
  
	private String productVersion;

	private String startTime;

    private String finishTime;

    public Releasecycle() {
  		super();
  		// TODO Auto-generated constructor stub
  	}

    
    public Releasecycle(String productVersion, String startTime, String finishTime) {
		super();
		this.productVersion = productVersion;
		this.startTime = startTime;
		this.finishTime = finishTime;
	}
    
    
    public String getProductVersion() {
        return productVersion;
    }

    public void setProductVersion(String productVersion) {
        this.productVersion = productVersion == null ? null : productVersion.trim();
    }

    public String getstartTime() {
        return startTime;
    }

    public void setstartTime(String startTime) {
        this.startTime = startTime == null ? null : startTime.trim();
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime == null ? null : finishTime.trim();
    }
    
    @Override
	public String toString() {
		return "Releasecycle [productVersion=" + productVersion + ", startTime=" + startTime + ", finishTime="
				+ finishTime + "]";
	}
}