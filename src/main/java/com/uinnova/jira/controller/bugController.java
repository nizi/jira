package com.uinnova.jira.controller;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.Data;

import org.apache.http.impl.bootstrap.HttpServer;
import org.apache.ibatis.session.SqlSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mysql.cj.x.protobuf.MysqlxCrud.Collection;
import com.uinnova.jira.JiraData.GetBugData;
import com.uinnova.jira.model.Bug;
import com.uinnova.jira.model.Releasecycle;
import com.uinnova.jira.service.BugService;
import com.uinnova.jira.service.ReleaseCycleService;
import com.uinnova.jira.util.MapUtil;
import com.uinnova.jira.util.StrUtil;



@Controller
//@RestController
@EnableScheduling
@MapperScan("com.uinnova.jira.mapper")
public class bugController {
	
	@Autowired 
	BugService bugService;

	@Autowired
	ReleaseCycleService rcService;
	
	@RequestMapping(value="/list")
	public String index(Model model){
		model.addAttribute("name", "nizi");
		return "index";
	}
	
	//获取-bug总量曲线
	@RequestMapping(value="/getTotalByProductAndVersion",method = RequestMethod.GET)
	public String getByPnameAndVersion(Map<String , Object> map) {
		
		//获取版本号
		String[] vs  = bugService.getAllVersion();
		String[] verSort = MapUtil.sortMap(vs);
		
		//获取产品线名称
		String[] pNames = bugService.getAllProductNames();
		System.out.println(pNames);
		
		//获取各产品，各版本bug
		Map<String,	Object> mp= new HashMap<String,Object>();
		for(int i=0;i<pNames.length;i++) {
			
			String pName = pNames[i];
			Map<String, Integer> pt = bugService.getBugTotalByProductNameAndVersion(pName, vs);
			mp.put(pName, pt);
		}
		
//		map.put("xVersion", vs);
		map.put("xVersion", verSort);
		map.put("productName",pNames);
		map.put("productBug", mp);
		
		return "getTotalByProductAndVersion";
	}
	
	//获取-各产品线bug总量曲线
	@RequestMapping(value="/getTotalByVersion",method = RequestMethod.GET)
	public String getByVersion(Map<String , Object> map) {
		
		//获取版本号
		String[] vs = bugService.getAllVersion();
		
		//获取对应值
		Map<String, Integer> pt = bugService.getBugTotalByProductNameAndVersion(null, vs);
		
		//2019.12.26:将版本号排序,并将bugcount按照版本号顺序重组
		String[] verArr = MapUtil.sortMap(vs);
		map.put("xVersion", verArr);
		
		map.put("bugCount", pt);
		return "getTotalByVersion";
	}

	//获取-bug修复周期
	@RequestMapping(value="/getFixTime")
//	@ResponseBody
	public String getFixTime(Map<String, Object> map) {
		ArrayList<String> ver = new ArrayList<String>();
		ArrayList<String> fts = new ArrayList<String>();
		
		//获取各版本周期
		List<Releasecycle> rcList = rcService.getAll();
		 List<Map<String, Object>> ftList = bugService.getFixedTime();
		 System.out.println(ftList.toString());
		 
		 //版本号排序
		 ArrayList<String> verArr = new ArrayList();
		 for(int i=0;i<ftList.size();i++) {
				String pv = ((String) ftList.get(i).get("Product_version")).split("_")[1];
				verArr.add(pv);
		 }
		 String[] verSort = MapUtil.sortVer(verArr);
		 System.out.println(verSort);
		 
		 for(int i=0;i<verSort.length;i++) {
			 for(int j=0;j<ftList.size();j++) {
				 if(ftList.get(j).get("Product_version").equals("Tarsier_"+verSort[i])) {
					String pv = (String) ftList.get(j).get("Product_version");
					String ft =  ftList.get(j).get("avgfixtime").toString();
					ver.add(pv);
					fts.add(ft);
					break;
				}
			 
		 }
		 }
		
		//计算各版本修复周期
		map.put("xVersion", ver);
		map.put("fixtime", fts);
		return "getFixTime";
	}
	
	//获取后6天 bug创建及修复量
	@RequestMapping(value="/getSomeDaysPercent/{days}")
	public String getLastSixDaysPercent(@PathVariable(name = "days") int days,Model model) {
		List<Map<String, Object>> ls= bugService.getLastSomeDaysBugPer(days);
		
		//获取db返回值
		System.out.println("===key==="+ls.get(0).get("res"));
		
		//将字符串转换为str[]
		JSONArray ja = StrUtil.str2JsonArray((String)ls.get(0).get("res"));
		JSONArray sortArr = StrUtil.sortJsonArrayByVersion(ja);
		
		//对JSONArray结果进行排序
		System.out.println(sortArr);

		model.addAttribute("res", sortArr.toString());
//		model.addAttribute("res", ls.get(0).get("res").toString());
		model.addAttribute("days", days);
		return "getLastSomeDays";
	}
	
	//获取有效率和修复率
	@RequestMapping(value="/getValAndFixPer")
	public String selectFixAndValidBugsPer(Map<String,Object> map) {
		
		List<Map<String, Object>> ls= bugService.getFixAndValidBugsPer();
		
		//获取db返回值
		System.out.println("===key==="+ls.get(0).get("res"));
		
		//将字符串转换为str[]
		JSONArray ja = StrUtil.str2JsonArray((String)ls.get(0).get("res"));
		JSONArray sortArr = StrUtil.sortJsonArrayByVersion(ja);
		
		//对JSONArray结果进行排序
		System.out.println(sortArr);
		map.put("res",sortArr.toString());
		
//		map.put("res", ls.get(0).get("res"));

		return "getValidAndFixPer";
	}
	
	//获取提测质量
	@RequestMapping(value="/getPreTestQuality")
	public String selectPreTestQuality(Map<String, Object> map) {
		
			String ver = null;
			int num = 0;
			
			//获取所有产品线名称
			String[] pnList = bugService.getAllProductNames();
			System.out.println("产品线名称::"+pnList.toString());
			
			//获取版本号
			String[] pvList = rcService.getAllVers();
			System.out.println("版本号为::"+pnList.toString());
			
			JSONObject pObj =  new JSONObject();
			
			for (int i = 0; i < pnList.length; i++) {
				//获取单条产品线数据,组装{产品：数值组}
				JSONObject obj =  new JSONObject();
				List<Map<String, Object>> ls= bugService.getPreTestQuality(pnList[i]);
				System.out.println("产品为："+pnList[i]);
				ArrayList<String> nList = new ArrayList<String>();
				System.out.println("单条产品线数据::"+ls);
				for(int j=0;j<ls.size();j++) {
					ver = ls.get(j).get("Product_version").toString();
					num = Integer.parseInt(ls.get(j).get("num").toString());
					try {
						obj.put(ver, num);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				try {
					pObj.put(pnList[i], obj);
				} catch (JSONException e) {
					e.printStackTrace();
				}
	}
			//版本号排序
			ArrayList<String> al = new ArrayList();
			for(int i=0;i<pvList.length;i++) {
				al.add(((String)pvList[i]).split("_")[1]);
			}
			String[] verSort = MapUtil.sortVer(al);
			for(int i=0;i<verSort.length;i++) {
				verSort[i]="Tarsier_"+verSort[i];
			}
			
			System.out.println("xVersion::"+verSort);
			System.out.println("pName::"+pnList);
			System.out.println("num::"+pObj.toString());
			map.put("xVersion", verSort);
			map.put("pName", pnList);
			map.put("num", pObj.toString());
			
			return "getPreTestQuality";
}
		//按照版本号获取各产品线优先级
	@RequestMapping(value="/getPriorityByVersion",method=RequestMethod.GET)
	public String getPriorityByVersion(@RequestParam("version") String ver,Model model) {
//		public String getPriorityByVersion(@PathVariable(name = "version") String ver,Model model) {
		
		List<Map<String,Object>> gpLi= bugService.getPriorityByVersion(ver);
		System.out.println("查询结果为："+gpLi.toString());
		
		String[] pnList= bugService.getAllProductNames();
		System.out.println("产品线为："+pnList.toString());
		
		
		//获取优先级类型
		List<Map<String, String>> priType = bugService.getPriorityType();
		String pStr = (priType.get(0).toString().split("="))[1];
		
		String[] priTypeLi = pStr.substring(0, pStr.length()-1) .split(",");
		
		//按照优先级分组
		JSONObject obj = new JSONObject();
		String name = null;
		int num = 0;
		for(int i=0;i<priTypeLi.length;i++) {
			
			String pri = priTypeLi[i];
			System.out.println("当前优先级为：："+pri);
			
			JSONObject subObj = new JSONObject();
			for(int j=0;j<gpLi.size();j++) {
				//
				if(gpLi.get(j).get("priority").toString().contains(pri)) {
					name = gpLi.get(j).get("project_name").toString();
					num = Integer.parseInt(gpLi.get(j).get("num").toString());
					try {
						subObj.put(name, num);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
			try {
				System.out.println("此优先级结果为："+subObj);
				obj.put(pri, subObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("结果为：："+obj);
		System.out.println("产品线名称为：："+pnList);
		System.out.println("优先级类型为：："+priTypeLi);
		
		model.addAttribute("priType", priTypeLi);
		model.addAttribute("pName", pnList);
		model.addAttribute("Res", obj.toString());
		model.addAttribute("ver", ver);
		
		return "getPriorityByVersion";
	}
	
	//获取每日创建及修复bug数
	@RequestMapping(value="/getCreateAndFixBugsByDay")
	public String getCreateAndFixBugsByDay(@RequestParam("version") String ver,Model model) {
		
		//String ver = "Tarsier_5.2.0";
		//获取当前版本的日期分布
		List<Map<String, String>> dList = bugService.getVersionDays(ver);
		System.out.println("版本周期为："+dList);
		
		ArrayList<String> dStr = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String da = null;
		
		for(int i=0;i<dList.size();i++) {
			 da = sdf.format(dList.get(i).get("tm"));
			 System.out.println("日期为："+da);
			dStr.add(da);
		}
		System.out.println("版本时间为："+dStr);
		
		
		//获取当前版本的创建数据
		List<Map<String, Object>> clList = bugService.getCreateNum(ver);
		System.out.println("每日创建数据："+clList);
		JSONObject cObject =  new JSONObject();
		String cd = null;
		int cnum = 0;
		for(int i=0;i<clList.size();i++) {
			cd = sdf.format(clList.get(i).get("create_time"));
			cnum = Integer.parseInt(clList.get(i).get("cnum").toString());
			try {
				cObject.put(cd, cnum);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("创建db："+cObject);
		
		//获取当前版本的修复数据
		List<Map<String, Object>> ulList = bugService.getFixedNum(ver);
		System.out.println("每日修复的数据："+ulList);
		JSONObject uObject =  new JSONObject();
		String ud = null;
		int unum = 0;
		for(int i=0;i<ulList.size();i++) {
			if(ulList.get(i).get("fixed_time")!=null) {
				ud = sdf.format(ulList.get(i).get("fixed_time"));
				unum = Integer.parseInt(ulList.get(i).get("unum").toString());
				try {
					uObject.put(ud, unum);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			
		}
		System.out.println("修复db："+uObject);
		
		JSONObject pObject = new JSONObject();
		try {
			pObject.put("create_time", cObject);
			pObject.put("fixed_time", uObject);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		String[] lineType = {"create_time","fixed_time"};
		model.addAttribute("xVersion",dStr);
		model.addAttribute("lineType",lineType);
		model.addAttribute("res",pObject.toString());
		
		return "getCreateAndFixBugsByDay";
	}
	
	
	//获取开发人员名下bug
	@RequestMapping(value="/getBugsByDev")
	public String getBugsByDev(@RequestParam("version") String ver,Model model) {
		
		List<Map<String, Object>> dlist  = null;
		if(!ver.contains("All")) {
			dlist = bugService.getBugsByDev(ver);;
			System.out.println("当前版本测试人员分布："+dlist.toString());
		}else {
			dlist = bugService.getBugsByDevForYears();
			System.out.println("全年测试人员分布："+dlist.toString());
		}
		
//		List<Map<String, Object>> dlist  = bugService.getBugsByDev(ver);
		System.out.println("开发人员分布："+dlist.toString());

		String dev = null;
		int num = 0;
		
		ArrayList<String> devs = new ArrayList<String>();
		ArrayList<Integer> nums = new ArrayList<Integer>();
		
		for(int i=0;i<dlist.size();i++) {
			dev=dlist.get(i).get("developer").toString();
			num=Integer.parseInt(dlist.get(i).get("num").toString());
			devs.add(dev);
			nums.add(num);
		}
		model.addAttribute("xAxis"+ devs);
		System.out.println("nums"+nums);
		
		model.addAttribute("xAxis", devs);
		model.addAttribute("res", nums);
		model.addAttribute("ver", ver);
		return "getBugsByDev";
	}
	
	//获取测试人员名下bug
	@RequestMapping(value="/getBugsByReporter")
	public String getBugsByReporter(@RequestParam("version") String ver,Model model) {
		
		List<Map<String, Object>> tlist = null;
		if(!ver.contains("All")) {
			tlist = bugService.getBugsByReporter(ver);
			System.out.println("当前版本测试人员分布："+tlist.toString());
		}else {
			tlist = bugService.getBugsByReporterForYears();
			System.out.println("全年测试人员分布："+tlist.toString());
		}
		System.out.println("测试人员分布："+tlist.toString());

		String re = null;
		int num = 0;
		
		ArrayList<String> rs = new ArrayList<String>();
		ArrayList<Integer> nums = new ArrayList<Integer>();
		
		for(int i=0;i<tlist.size();i++) {
			re=tlist.get(i).get("reporter").toString();
			num=Integer.parseInt(tlist.get(i).get("num").toString());
			rs.add(re);
			nums.add(num);
		}
		model.addAttribute("xAxis"+ rs);
		System.out.println("nums"+nums);
		
		model.addAttribute("xAxis", rs);
		model.addAttribute("res", nums);
		model.addAttribute("ver", ver);
		return "getBugsByReporter";
	}
	
	
	//获取测试人员名下bug
		@RequestMapping(value="/getOpenedBugs")
		public String getOpenedBugs(@RequestParam("version") String ver,Model model) {
			List<Map<String,Object>> map =bugService.getOpenedBugs(ver);
			System.out.println(map);
			ArrayList<Integer> ns = new ArrayList<Integer>();
			ArrayList<String> pns = new ArrayList<String>();
			
			int num = 0;
			String pname = null;
			for(int i=0;i<map.size();i++) {
				num = Integer.parseInt(map.get(i).get("num").toString());
				pname = map.get(i).get("project_name").toString();
				ns.add(num);
				pns.add(pname);
			}
			int sum = 0;
			for(int j=0;j<ns.size();j++) {
				sum +=ns.get(j);
			}
			ns.add(sum);
			pns.add("总量");
			
			
			model.addAttribute("xAxis", pns);
			model.addAttribute("res", ns);
//			model.addAttribute("ver", ver);
			return "getOpenedBugsByVersion";
		}
	
	
////	===================手动同步jira数据（schedule秒周期也可实现）====================================
	@RequestMapping(value="/getData")
	public String insertDataToDb() {
		
//		获取jirabug
//		String host = "http://111.204.156.11:8070/";
		String host = "http://192.168.1.199:8080/";
		
		ArrayList<Bug> list = null;
		
//		插入db
		try {
			list = GetBugData.getAllBugsData(host);
			System.out.println("===========************==========insert start=================**********================");
				for (int i=0;i<list.size();i++) {
					System.out.println("****insertDB**"+list.get(i).getId());
					//添加数据
//					bugService.addBug(list.get(i));
					//更新数据
					bugService.updateBug(list.get(i));
					System.out.println("========update====");
					Thread.currentThread().sleep(10);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 

		System.out.println("sucess");
		return "async";
	}

}